package com.mq.mqdemo.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Queue;

@RestController
@RequestMapping("/rest/publish")
public class ProducerResource {
    JmsTemplate jmsTemplate;

    final
    Queue queue;

    public ProducerResource(Queue queue) {
        this.queue = queue;
    }

    @Autowired
    public ProducerResource(JmsTemplate jmsTemplate, Queue queue) {
        this.jmsTemplate = jmsTemplate;
        this.queue = queue;
    }

    @GetMapping("/{message}")
    public String publish(@PathVariable("message")
                          final String message) {

        jmsTemplate.convertAndSend(String.valueOf(queue),message);
        return "Published Successfully";

    }
}
