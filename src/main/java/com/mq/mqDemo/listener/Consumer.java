package com.mq.mqdemo.listener;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.Destination;

@Component
public class Consumer {
    @JmsListener(destination = "standalone.queue")
    public void consume(String message){
        System.out.println("Received Message: " + message);
    }
}
